package com.ftl.notebook.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ftl.core.config.CoreConfig;

/**
 * Java Config Spring Context for Notebook Model.
 * 
 * @author Petros Siatos
 *
 */
@Configuration
@ComponentScan({NotebookModelConfig.PACKAGE_ROOT})
@EnableJpaRepositories(basePackages = {NotebookModelConfig.PACKAGE__DAO}, repositoryImplementationPostfix = NotebookModelConfig.REPOSITORY_IMPL_POSTFIX)
@Import({ CoreConfig.class, HibernateConfig.class })
public class NotebookModelConfig {
	
	public static final String PACKAGE_ROOT = "com.ftl.notebook.core";
	public static final String PACKAGE__DAO = "com.ftl.notebook.core.dao";
	public static final String REPOSITORY_IMPL_POSTFIX = "Impl";
}
