package com.ftl.notebook.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.ftl.notebook.core.service.INotebookOakRepository;
import com.ftl.notebook.core.service.NotebookOakRepoFileSystemImpl;
import com.ftl.notebook.core.service.NotebookOakRepoMongoImpl;
import com.ftl.oak.model.OakFileSystemPropertyDto;
import com.ftl.oak.model.OakMongoPropertyDto;
import com.mongodb.MongoClientURI;

/**
 * Java Config Spring Context for Notebook Model.
 * 
 * @author Petros Siatos
 *
 */
@Configuration
public class NotebookOakConfig {
	

	// Repo Constants
	private static final String REPO_NAME = "notebook";
	
	private static final String PROPERTY_OAK_PATH = "oak.path";
	private static final String PROPERTY_OAK_MAX	= "oak.max.size";
//	private static final String PROPERTY_OAK_MONGO_HOST = "oak.mongo.host";
//	private static final String PROPERTY_OAK_MONGO_PORT = "oak.mongo.port";
//	private static final String PROPERTY_OAK_MONGO_NAME = "oak.mongo.name";
	private static final String PROPERTY_OAK_MONGO_URI = "oak.mongo.uri";
	
	@Autowired
	private Environment env;
	
//	@Bean
	public INotebookOakRepository notebookOakRepoFileSystemImpl() {
		OakFileSystemPropertyDto oakDto = new OakFileSystemPropertyDto();
		oakDto.setName(REPO_NAME);
		oakDto.setPath(System.getProperty("user.dir") + "/" + env.getProperty(PROPERTY_OAK_PATH));
		oakDto.setMaxFileSize(Integer.valueOf(env.getProperty(PROPERTY_OAK_MAX)));
		return new NotebookOakRepoFileSystemImpl(oakDto);
	}
	
	@Bean
	public INotebookOakRepository mongoNotebookOakRepoImpl() {
		OakMongoPropertyDto oakDto = new OakMongoPropertyDto(new MongoClientURI(env.getProperty(PROPERTY_OAK_MONGO_URI)));
		return new NotebookOakRepoMongoImpl(oakDto);
	}
}
