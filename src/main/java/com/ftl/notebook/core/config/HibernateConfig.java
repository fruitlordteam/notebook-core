package com.ftl.notebook.core.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ftl.core.utils.CoreConstant;

@Configuration
@EnableTransactionManagement
public class HibernateConfig {

	public static final String[] PATH_ENTITIES = new String[] {"com.ftl.core.model", "com.ftl.notebook.core.model", "com.ftl.oak.model"};
	public static final String BEAN_DATASOURCE = "dataSource";
	
	private static final String PROPERTY_DB_URL 	= "db.url";
	private static final String PROPERTY_DB_USER 	= "db.user";
	private static final String PROPERTY_DB_PASS	= "db.pass";
	
	private static final String PROPERTY_DB_SHOW_SQL	= "db.show.sql";
	private static final String PROPERTY_DB_AUTO_DDL	= "db.hibernate.auto";
	
	private static final String HEROKU_PROPERTY_DB_URL	= "DATABASE_URL";
	
	@Autowired
	private Environment env;
	
	@Bean(name = BEAN_DATASOURCE)
	@Profile(CoreConstant.PROFILE_DEVELOPMENT)
	public DataSource dataSource() {

		PGPoolingDataSource ds = new PGPoolingDataSource();
//		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUser(env.getProperty(PROPERTY_DB_USER));
		ds.setPassword(env.getProperty(PROPERTY_DB_PASS));
		ds.setUrl(env.getProperty(PROPERTY_DB_URL));
		return ds;
	}
	
	@Bean(name = BEAN_DATASOURCE)
	@Profile(CoreConstant.PROFILE_HEROKU)
	/**
	 * Datasource for Deployment on Heroku
	 * https://devcenter.heroku.com/articles/connecting-to-relational-databases-on-heroku-with-java
	 * @return
	 * @throws URISyntaxException
	 */
	public DataSource herokuDataSource() throws URISyntaxException {

		URI dbUri = new URI(System.getenv(HEROKU_PROPERTY_DB_URL));
        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
        
        PGPoolingDataSource ds = new PGPoolingDataSource();
		ds.setUrl(dbUrl);
		ds.setUser(username);
		ds.setPassword(password);
		
		return ds;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//		vendorAdapter.setGenerateDdl(true);
//		vendorAdapter.setShowSql(env.getProperty(PROPERTY_DB_SHOW_SQL));
		
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		// TODO: find a better way to include other project entities
		em.setPackagesToScan(PATH_ENTITIES);
//		em.setMappingResources("META-INF/orm.xml");
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}
	
	private Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty(org.hibernate.cfg.Environment.HBM2DDL_AUTO, env.getProperty(PROPERTY_DB_AUTO_DDL));
		properties.setProperty(org.hibernate.cfg.Environment.SHOW_SQL, env.getProperty(PROPERTY_DB_SHOW_SQL));
		properties.setProperty(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.PostgreSQL94Dialect");
		return properties;
	}
	
}
