package com.ftl.notebook.core.dao;

import com.ftl.core.dao.ICoreDao;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;

public interface CourseDao extends ICoreDao<Course>, CourseDaoCustom {
	
	public Iterable<Course> findByDepartment(Department dept);
	public Iterable<Course> findByDepartmentCode(String deptCode);
	public Course findByCode(String code);
	
}
