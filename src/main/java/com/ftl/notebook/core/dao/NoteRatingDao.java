package com.ftl.notebook.core.dao;


import java.util.Optional;

import com.ftl.core.dao.ICoreDao;
import com.ftl.core.model.CoreUser;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.note.NoteRating;

public interface NoteRatingDao extends ICoreDao<NoteRating> {

	public Optional<NoteRating> findByNoteAndUser(Note note, CoreUser user);
	
}