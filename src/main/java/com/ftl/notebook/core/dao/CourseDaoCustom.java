package com.ftl.notebook.core.dao;

import java.util.Optional;

import com.ftl.core.dao.ICoreDaoCustom;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;

public interface CourseDaoCustom extends ICoreDaoCustom<Course> {
	
	public Optional<Course> getByDepartment(Department department, String code);
	public Optional<Course> getWithNotes(Department department, String code);
	
}
