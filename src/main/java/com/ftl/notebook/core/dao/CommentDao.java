package com.ftl.notebook.core.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ftl.core.dao.ICoreDao;
import com.ftl.notebook.core.model.note.Comment;

public interface CommentDao extends ICoreDao<Comment> {
	
	public Optional<Comment> findById(Long id);
	
	public Page<Comment> findByNote_Id(Pageable pageable, Long noteId);
	
}