package com.ftl.notebook.core.dao;

import java.util.Optional;

import com.ftl.core.dao.AbstractCoreDaoCustomImpl;
import com.ftl.core.model.CoreParam;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;

public class DepartmentDaoImpl extends AbstractCoreDaoCustomImpl<Department> implements DepartmentDaoCustom {

	@Override
	public Class<Department> getModel() {
		return Department.class;
	}
	
	@Override
	public Optional<Department> get(University university, String deptCode) {
		return getByNaturalID(
				CoreParam.createCoreParam(Department.UNIVERSITY, university), 
				CoreParam.createCoreParam(Department.CODE, deptCode));
	}
	
	@Override
	public Optional<Department> getWithCourses(University university, String deptCode) {
		getSession().enableFetchProfile(Department.FETCH_PROFILE_WITH_COURSES);
		return getByNaturalID(
				CoreParam.createCoreParam(Department.UNIVERSITY, university), 
				CoreParam.createCoreParam(Department.CODE, deptCode));
	}

}
