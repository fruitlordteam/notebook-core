package com.ftl.notebook.core.dao;

import com.ftl.core.dao.ICoreDao;
import com.ftl.notebook.core.model.university.University;

public interface UniversityDao extends ICoreDao<University>, UniversityDaoCustom {

	public University findByCode(String uniCode);
	
}