package com.ftl.notebook.core.dao;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ftl.core.dao.ICoreDao;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.university.Course;

public interface NoteDao extends ICoreDao<Note> {

//	@EntityGraph(value = Note.ENTITY_GRAPH_WITH_CREATED_BY, type = EntityGraphType.LOAD)
//	public Optional<Note> findById(Long id);
	
//	@EntityGraph(value = Note.ENTITY_GRAPH_WITH_COMMENTS, type = EntityGraphType.LOAD)
	public Optional<Note> findById(Long id);
	
	public Page<Note> findByCourse_Id(Pageable pageable, Long courseId);
	
	public Iterable<Note> findByCourse(Course course);
	public Iterable<Note> findByCourseCode(String courseCode);
	
}