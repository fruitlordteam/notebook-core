package com.ftl.notebook.core.dao;

import java.util.Optional;

import com.ftl.core.dao.ICoreDaoCustom;
import com.ftl.notebook.core.model.university.University;

public interface UniversityDaoCustom extends ICoreDaoCustom<University> {
	
	public Optional<University> getByCode(String uniCode);
	public Optional<University> getByCodeWithDepartments(String uniCode);
	public Boolean existsByCode(String code);
	
}
