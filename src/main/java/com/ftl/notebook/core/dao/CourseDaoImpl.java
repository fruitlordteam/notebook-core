package com.ftl.notebook.core.dao;

import java.util.Optional;

import com.ftl.core.dao.AbstractCoreDaoCustomImpl;
import com.ftl.core.model.CoreParam;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;

public class CourseDaoImpl extends AbstractCoreDaoCustomImpl<Course> implements CourseDaoCustom {

	@Override
	public Class<Course> getModel() {
		return Course.class;
	}
	
	@Override
	public Optional<Course> getWithNotes(Department department, String code) {
		getSession().enableFetchProfile(Course.FETCH_PROFILE_WITH_NOTES);
		return getByNaturalID(
				CoreParam.createCoreParam(Course.DEPARTMENT, department), 
				CoreParam.createCoreParam(Course.CODE, code));
	}

	@Override
	public Optional<Course> getByDepartment(Department department, String code) {
		return getByNaturalID(
				CoreParam.createCoreParam(Course.DEPARTMENT, department), 
				CoreParam.createCoreParam(Course.CODE, code));
	}

}