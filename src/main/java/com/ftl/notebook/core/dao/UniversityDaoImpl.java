package com.ftl.notebook.core.dao;

import java.util.Optional;

import com.ftl.core.dao.AbstractCoreDaoCustomImpl;
import com.ftl.core.model.CoreParam;
import com.ftl.notebook.core.model.university.University;

public class UniversityDaoImpl extends AbstractCoreDaoCustomImpl<University> implements UniversityDaoCustom {

	@Override
	public Class<University> getModel() {
		return University.class;
	}
	
	@Override
	public Optional<University> getByCode(String uniCode) {
		return getBySimpleNaturalID(uniCode);
	}
	
	@Override
	public Optional<University> getByCodeWithDepartments(String uniCode) {
		getSession().enableFetchProfile(University.FETCH_PROFILE_WITH_DEPARTMENTS);
		return getBySimpleNaturalID(uniCode);
	}

	@Override
	public Boolean existsByCode(String code) {
		return existsByAttribute(CoreParam.createCoreParam(University.CODE, code));
	}

}