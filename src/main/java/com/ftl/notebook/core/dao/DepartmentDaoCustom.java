package com.ftl.notebook.core.dao;

import java.util.Optional;

import com.ftl.core.dao.ICoreDaoCustom;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;

public interface DepartmentDaoCustom extends ICoreDaoCustom<Department> {
	
	public Optional<Department> get(University university, String code);
	public Optional<Department> getWithCourses(University university, String code);
	
}
