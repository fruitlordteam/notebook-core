package com.ftl.notebook.core.dao;

import com.ftl.core.dao.ICoreDao;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;

public interface DepartmentDao extends ICoreDao<Department>, DepartmentDaoCustom {
	
	public Iterable<Department> findByUniversity(University university);
	public Iterable<Department> findByUniversityId(Long id);
	public Iterable<Department> findByUniversityCode(String code);
	public Department findByCode(String code);
	
}