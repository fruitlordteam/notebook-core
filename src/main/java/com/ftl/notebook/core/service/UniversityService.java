package com.ftl.notebook.core.service;

import java.util.Optional;

import javax.jcr.RepositoryException;

import org.springframework.stereotype.Service;

import com.ftl.core.model.CoreModel;
import com.ftl.core.service.IUniqueModelService;
import com.ftl.notebook.core.dao.UniversityDao;
import com.ftl.notebook.core.model.university.University;

@Service
public class UniversityService extends NotebookRepoNodeService<University, UniversityDao> implements IUniqueModelService {

	@Deprecated
	public University findByCode(String uniCode) {
		return getDao().findByCode(uniCode);
	}
	
	public Optional<University> getByCode(String uniCode) {
		return getDao().getByCode(uniCode);
	}
	
	public Optional<University> getByCodeWithDepartments(String uniCode) {
		return getDao().getByCodeWithDepartments(uniCode);
	}
	
	@Override
	public void addNode(University node) throws RepositoryException {
		  repo.addUniversity(node);
	}

	@Override
	public Boolean exists(CoreModel model) {
		return getDao().existsByCode(((University) model).getCode());
	}
	
}