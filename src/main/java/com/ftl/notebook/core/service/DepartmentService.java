package com.ftl.notebook.core.service;

import java.util.Optional;

import javax.jcr.RepositoryException;

import org.springframework.stereotype.Service;

import com.ftl.notebook.core.dao.DepartmentDao;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;

@Service
public class DepartmentService extends NotebookRepoNodeService<Department, DepartmentDao> {

	public Iterable<Department> findByUniversity(University university) {
		 return getDao().findByUniversity(university);	
	}
	
//	public Iterable<Department> findByUniversityId(Long uniId) {
//		 return getDao().findByUniversityId(uniId);	
//	}
	
	public Iterable<Department> findByUniversityCode(String code) {
		 return getDao().findByUniversityCode(code);	
	}
	
	public Department findByCode(String code) {
		return getDao().findByCode(code);
	}
	
	@Override
	public void addNode(Department node) throws RepositoryException {
		  repo.addDepartment(node);
	}
	
	public Optional<Department> get(University university, String code) {
		return getDao().get(university, code);
	}

	public Optional<Department> getWithCourses(University university, String code) {
		return getDao().getWithCourses(university, code);
	}

}