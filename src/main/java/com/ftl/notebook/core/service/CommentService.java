package com.ftl.notebook.core.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftl.core.service.CoreService;
import com.ftl.notebook.core.dao.CommentDao;
import com.ftl.notebook.core.model.note.Comment;

@Service
public class CommentService extends CoreService<Comment, CommentDao>{
	
	public Optional<Comment> findById(Long id) {
		return getDao().findById(id);
	}
	
	public Page<Comment> findByNote_Id(Pageable pageable, Long noteId) {
		return getDao().findByNote_Id(pageable, noteId);
	}
	
	public void addComment() {
		
	}
	
}
