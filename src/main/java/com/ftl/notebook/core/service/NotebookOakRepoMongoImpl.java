package com.ftl.notebook.core.service;

import java.io.IOException;
import java.util.Calendar;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;

import org.apache.jackrabbit.JcrConstants;

import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;
import com.ftl.oak.model.OakMultipartFile;
import com.ftl.oak.model.OakMongoPropertyDto;
import com.ftl.oak.service.OakRepoImplMongo;

/**
 * Notebook Jcr Map: 
 * (/notebook/universities/uniCode
 * 		/departments/deptCode
 * 			/courses/courseCode
 * 				/notes/noteId)
 * @author Petros Siatos
 *
 */
public class NotebookOakRepoMongoImpl extends OakRepoImplMongo implements INotebookOakRepository {

	// TODO: Remove Duplication NotebookOakRepoMongoImpl - NotebookOakRepoFileSystemImpl
	
	// Repo Folders
	public static final String FOLDER_UNIVERSITIES = "universities";
	public static final String FOLDER_DEPARTMENTS = "departments";
	public static final String FOLDER_COURSES		= "courses";
	public static final String FOLDER_NOTES 		= "notes";
	
	public NotebookOakRepoMongoImpl(OakMongoPropertyDto oakDto) {
		super(oakDto);
	}
	
	// TODO: Add Spring AOP (login logout)
	public void addUniversity(University university) throws RepositoryException {
		Node root = getRoot();
		Node universitiesFolder = root.getNode(FOLDER_UNIVERSITIES);
		
		/**
		 * Create new university folder and it's departments folder.
		 * ( /universities/uniCode/departments/ )
		 */
		Node newUniversityFolder = universitiesFolder.addNode(university.getNodeName(), NodeType.NT_FOLDER);
		newUniversityFolder.addNode(FOLDER_DEPARTMENTS, NodeType.NT_FOLDER);
	}
	
	// TODO: Add Spring AOP (login logout)
	public void addDepartment(Department dept) throws RepositoryException {
		Node root = getRoot();
		Node universityFolder = root.getNode(dept.getUniversity().getNodeFullPath());
		Node departmentsFolder = universityFolder.getNode(FOLDER_DEPARTMENTS);
		
		/**
		 * Create new department folder and it's courses folder.
		 * ( /departments/deptCode/courses )
		 */
		Node newDepartmentFolder = departmentsFolder.addNode(dept.getNodeName(), NodeType.NT_FOLDER);
		newDepartmentFolder.addNode(FOLDER_COURSES, NodeType.NT_FOLDER);
	}
	
	
	// TODO: Add Spring AOP (login logout)
	public void addCourse(Course course) throws RepositoryException {
		Node root = getRoot();
		Node departmentFolder = root.getNode(course.getDepartment().getNodeFullPath());
		Node coursesFolder	= departmentFolder.getNode(FOLDER_COURSES);
		
		/**
		 * Create new course folder and it's notes folder.
		 * ( /courses/courseCode/notes )
		 */
		Node newCourseFolder = coursesFolder.addNode(course.getNodeName(), NodeType.NT_FOLDER);
		newCourseFolder.addNode(FOLDER_NOTES, NodeType.NT_FOLDER);
	}
	
	// TODO: Add Spring AOP (login logout)
	public void addNote(Note note) throws RepositoryException, IOException {
		assert(note.getFile() != null);
		Node root = getRoot();
		Node courseFolder = root.getNode(note.getCourse().getNodeFullPath());
		Node notesFolder = courseFolder.getNode(FOLDER_NOTES);
		
		Node newNoteFile = notesFolder.addNode(note.getNodeName(), NodeType.NT_FILE);
//		newNoteFile.addMixin("mix:referenceable");	// TODO: check this out
		Node resNode = newNoteFile.addNode (JcrConstants.JCR_CONTENT, NodeType.NT_RESOURCE);
		resNode.setProperty (JcrConstants.JCR_MIMETYPE, note.getFile().getMultipartFile().getContentType());
		resNode.setProperty(JcrConstants.JCR_DATA, note.getData());
		//resNode.setProperty (JcrConstants.JCR_ENCODING, fileDto.getEncoding());
		
		Calendar lastModified = Calendar.getInstance ();
		//lastModified.setTimeInMillis (file.lastModified ());
		resNode.setProperty (JcrConstants.JCR_LASTMODIFIED, lastModified);
		
		note.getFile().setIdentifier(resNode.getIdentifier());
	}
	
	public void retrieveNote(Note note) throws RepositoryException, IOException {
		login();
		Node node = getByPath(note.getNodeFullPath());
		note.getFile().setMultipartFile(new OakMultipartFile(node, note.getFile()));
		saveAndLogout();
	}

	@Override
	public void init() throws RepositoryException {
		login();
		Node root = getRoot();
		
		// Add University folder 
		if (!root.hasNode(FOLDER_UNIVERSITIES)) {
			System.out.println("Notebook Oak - Add University Folder");
			root.addNode(FOLDER_UNIVERSITIES, NodeType.NT_FOLDER);
		}
		
		saveAndLogout();
	}
	
}

