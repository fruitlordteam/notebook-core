package com.ftl.notebook.core.service;

import java.io.IOException;
import java.util.Optional;

import javax.jcr.RepositoryException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftl.notebook.core.dao.NoteDao;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.university.Course;

@Service
public class NoteService extends NotebookRepoNodeService<Note, NoteDao> {

	public Optional<Note> findById(Long id) {
		return getDao().findById(id);
	}
	
	public Iterable<Note> findByCourse(Course course) {
		 return getDao().findByCourse(course);	
	}

	public Iterable<Note> findByCourseCode(String courseCode) {
		 return getDao().findByCourseCode(courseCode);	
	}
	
	public Page<Note> findByCourse_Id(Pageable pageable, Long courseId) {
		return getDao().findByCourse_Id(pageable, courseId);
	}
	
	// IRepoNodeService implementation
	@Override
	public void addNode(Note node) throws RepositoryException, IOException {
		repo.addNote(node);
	}
	
	/**
	 * Retrieve note file from jcr repository and fill note.file.stream.
	 * @param note
	 * @return
	 * @throws RepositoryException
	 * @throws IOException 
	 */
	public void retrieveFromRepo(Note note) throws RepositoryException, IOException {
		repo.retrieveNote(note);
	}

}
