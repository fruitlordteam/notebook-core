package com.ftl.notebook.core.service;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.beans.factory.annotation.Autowired;

import com.ftl.core.dao.ICoreDao;
import com.ftl.core.model.CoreModel;
import com.ftl.core.service.CoreService;
import com.ftl.oak.model.IRepoNode;
import com.ftl.oak.service.IRepoNodeService;

/**
 * Notebook implementation of {@link IRepoNodeService}
 * Extend this service for models that implement {@link IRepoNode}
 * 
 * @author Petros Siatos
 *
 * @param <M> the {@link CoreModel} implementing {@link IRepoNode}
 * @param <D> the {@link ICoreDao}
 */
abstract class NotebookRepoNodeService<M extends CoreModel & IRepoNode, D extends ICoreDao<M>> extends CoreService<M, D> implements IRepoNodeService<M> {

	@Autowired INotebookOakRepository repo;
	
	// TODO: fix transaction.
	public void add(M model) throws RepositoryException, IOException {
		
		System.out.println("Add node." + model.getNodeFullPath() + model.getNodeName());
		
		save(model);
		
		try {
			repo.login();
			addNode(model);
		} catch(Exception ex) {
			System.out.println("Remove node." + model.getNodeFullPath() + model.getNodeName());
			removeNode(model);
			throw ex;
		} finally {
			repo.saveAndLogout();
		}
	}
	
	abstract public void addNode(M node) throws RepositoryException, IOException;

	public void remove(M model) throws RepositoryException {
		delete(model);
		repo.login();
		removeNode(model);
		repo.saveAndLogout();
	}

	public void removeNode(M model) throws RepositoryException {
		
		System.out.println("Remove node." + model.getNodeFullPath() + model.getNodeName());
		Node repoFolder = repo.getByPath(model.getNodeFullPath());
		repoFolder.remove();
	}

}
