package com.ftl.notebook.core.service;

import java.io.IOException;

import javax.jcr.RepositoryException;

import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;
import com.ftl.oak.service.IOakRepository;

public interface INotebookOakRepository extends IOakRepository {

	public void addUniversity(University university) throws RepositoryException;
	public void addDepartment(Department dept) throws RepositoryException;
	public void addCourse(Course course) throws RepositoryException;
	public void addNote(Note note) throws RepositoryException, IOException;
	public void retrieveNote(Note note) throws RepositoryException, IOException;
	
//	// TODO: Add Spring AOP (login logout)
//			public default void addUniversity(University university) throws RepositoryException {
//				Node root = getRoot();
//				Node universitiesFolder = root.getNode(FOLDER_UNIVERSITIES);
//				
//				/**
//				 * Create new university folder and it's departments folder.
//				 * ( /universities/uniCode/departments/ )
//				 */
//				Node newUniversityFolder = universitiesFolder.addNode(university.getNodeName(), NodeType.NT_FOLDER);
//				newUniversityFolder.addNode(FOLDER_DEPARTMENTS, NodeType.NT_FOLDER);
//			}
//			
//			// TODO: Add Spring AOP (login logout)
//			public default void addDepartment(Department dept) throws RepositoryException {
//				Node root = getRoot();
//				Node universityFolder = root.getNode(dept.getUniversity().getNodeFullPath());
//				Node departmentsFolder = universityFolder.getNode(FOLDER_DEPARTMENTS);
//				
//				/**
//				 * Create new department folder and it's courses folder.
//				 * ( /departments/deptCode/courses )
//				 */
//				Node newDepartmentFolder = departmentsFolder.addNode(dept.getNodeName(), NodeType.NT_FOLDER);
//				newDepartmentFolder.addNode(FOLDER_COURSES, NodeType.NT_FOLDER);
//			}
//			
//			
//			// TODO: Add Spring AOP (login logout)
//			public default void addCourse(Course course) throws RepositoryException {
//				Node root = getRoot();
//				Node departmentFolder = root.getNode(course.getDepartment().getNodeFullPath());
//				Node coursesFolder	= departmentFolder.getNode(FOLDER_COURSES);
//				
//				/**
//				 * Create new course folder and it's notes folder.
//				 * ( /courses/courseCode/notes )
//				 */
//				Node newCourseFolder = coursesFolder.addNode(course.getNodeName(), NodeType.NT_FOLDER);
//				newCourseFolder.addNode(FOLDER_NOTES, NodeType.NT_FOLDER);
//			}
//			
//			// TODO: Add Spring AOP (login logout)
//			public default void addNote(Note note) throws RepositoryException, IOException {
//				assert(note.getFile() != null);
//				Node root = getRoot();
//				Node courseFolder = root.getNode(note.getCourse().getNodeFullPath());
//				Node notesFolder = courseFolder.getNode(FOLDER_NOTES);
//				
//				Node newNoteFile = notesFolder.addNode(note.getNodeName(), NodeType.NT_FILE);
////				newNoteFile.addMixin("mix:referenceable");	// TODO: check this out
//				Node resNode = newNoteFile.addNode (JcrConstants.JCR_CONTENT, NodeType.NT_RESOURCE);
//				resNode.setProperty (JcrConstants.JCR_MIMETYPE, note.getFile().getMultipartFile().getContentType());
//				resNode.setProperty(JcrConstants.JCR_DATA, note.getData());
//				//resNode.setProperty (JcrConstants.JCR_ENCODING, fileDto.getEncoding());
//				
//				Calendar lastModified = Calendar.getInstance ();
//				//lastModified.setTimeInMillis (file.lastModified ());
//				resNode.setProperty (JcrConstants.JCR_LASTMODIFIED, lastModified);
//				
//				note.getFile().setIdentifier(resNode.getIdentifier());
//			}
//			
//			public default void retrieveNote(Note note) throws RepositoryException, IOException {
//				login();
//				Node node = getByPath(note.getNodeFullPath());
//				note.getFile().setMultipartFile(new OakMultipartFile(node, note.getFile()));
//				saveAndLogout();
//			}
//
//			public default void init() throws RepositoryException {
//				login();
//				Node root = getRoot();
//				
//				// Add University folder 
//				if (!root.hasNode(FOLDER_UNIVERSITIES)) {
//					System.out.println("Notebook Oak - Add University Folder");
//					root.addNode(FOLDER_UNIVERSITIES, NodeType.NT_FOLDER);
//				}
//				
//				saveAndLogout();
//			}
//		
	
}
