package com.ftl.notebook.core.service;

import java.util.Optional;

import javax.jcr.RepositoryException;

import org.springframework.stereotype.Service;

import com.ftl.notebook.core.dao.CourseDao;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;

@Service
public class CourseService extends NotebookRepoNodeService<Course, CourseDao> {

	public Iterable<Course> findByDepartment(Department dept) {
		return getDao().findByDepartment(dept);
	}

	public Iterable<Course> findByDepartmentCode(String deptCode) {
		return getDao().findByDepartmentCode(deptCode);
	}

	public Course findByCode(String code) {
		return getDao().findByCode(code);
	}
	
	@Override
	public void addNode(Course node) throws RepositoryException {
		  repo.addCourse(node);
	}
	
	public Optional<Course> getByDepartment(Department department, String code) {
		return getDao().getByDepartment(department, code);
	}

	public Optional<Course> getWithNotes(Department department, String code) {
		return getDao().getWithNotes(department, code);
	}
	
}