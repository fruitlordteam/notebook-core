package com.ftl.notebook.core.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftl.core.model.CoreUser;
import com.ftl.core.service.CoreService;
import com.ftl.notebook.core.dao.NoteRatingDao;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.note.NoteRating;

@Service
public class NoteRatingService extends CoreService<NoteRating, NoteRatingDao> {
	
	@Autowired 
	private NoteService noteService;
	
	public Optional<NoteRating> findByNoteAndUser(Note note, CoreUser user) {
		return getDao().findByNoteAndUser(note, user);
	}
	
	public void addRating(NoteRating noteRating) {
	
		noteRating.getNote().getRating().addStarRating(noteRating.getRating());
		
		noteService.save(noteRating.getNote());
		save(noteRating);
	}
}
