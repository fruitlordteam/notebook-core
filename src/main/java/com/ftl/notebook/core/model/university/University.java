package com.ftl.notebook.core.model.university;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfile.FetchOverride;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import com.ftl.core.annotation.validation.UniqueModel;
//import com.ftl.core.annotation.validation.UniqueConstraint;
import com.ftl.core.model.CoreModel;
import com.ftl.notebook.core.service.NotebookOakRepoFileSystemImpl;
import com.ftl.notebook.core.service.UniversityService;
import com.ftl.oak.model.IRepoNode;

@Entity
@Table(name = University.TABLE)
@UniqueModel(service = UniversityService.class)
@SequenceGenerator(name = CoreModel.ID_SEQ, sequenceName = University.SEQ, allocationSize = 1)
@FetchProfile(
	name = University.FETCH_PROFILE_WITH_DEPARTMENTS,
	fetchOverrides={
		@FetchOverride(
			entity=University.class,
			association= University.DEPARTMENTS,
			mode= FetchMode.JOIN
		)
	}
)
public class University extends CoreModel implements IRepoNode {
	
	public static final String TABLE		= "notebook_university";
	public static final String SEQ 			= "notebook_university_seq";
	
	// Entity Graphs / Fetch Profiles
	public static final String FETCH_PROFILE_WITH_DEPARTMENTS = "FP.w.depts";
	
	// Attributes
	public static final String DEPARTMENTS		= "departments";
	public static final String CODE				= "code";
	public static final String WEB_PAGE 		= "webPage";
	
	// Column Names
	public static final String COLUMN_WEB_PAGE 	= "web_page";
	
	@NotEmpty
	@NaturalId
	@Column
	protected String code;
	
	@Column(name = COLUMN_WEB_PAGE)
	@URL
	private String webPage;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = Department.UNIVERSITY)
	private Set<Department> departments = new HashSet<Department>();

	public University() {}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public String getWebPage() {
		return webPage;
	}

	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}
	
	public Set<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	// Utility Functions
	// IRepoNode implementation
	@Override
	public String getNodeName() {
		return code;
	}

	@Override
	public String getNodeFolderName() {
		return NotebookOakRepoFileSystemImpl.FOLDER_UNIVERSITIES;
	}
 
}
