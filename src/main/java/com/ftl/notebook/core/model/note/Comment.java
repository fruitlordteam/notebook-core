package com.ftl.notebook.core.model.note;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml.Tag;

import com.ftl.core.model.CoreModel;

@Entity
@Table(name= Comment.TABLE)
@SequenceGenerator(name = CoreModel.ID_SEQ, sequenceName = Comment.SEQ, allocationSize = 1)
public class Comment extends CoreModel {
	
	public static final String TABLE		= "notebook_comment";
	public static final String SEQ 			= "notebook_comment_seq";
	
	// Parameters
	public static final String NOTE 		= "note";
	public static final String LONG_DESC	= "longDesc";
	
	// Column Names
	public static final String COLUMN_LONG_DESC 	= "long_desc";
	
	public Comment() {}
	
	@NotNull
	@ManyToOne
//	@JsonIgnore
	private Note note;
	
	public Note getNote() {
		return note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	@NotEmpty
	@Tag(name = "BASIC_WITH_IMAGES")
	@Override
	public String getDescription() {
		return super.getDescription();
	}
	
}
