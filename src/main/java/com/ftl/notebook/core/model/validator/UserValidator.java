package com.ftl.notebook.core.model.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ftl.core.model.CoreUser;
import com.ftl.core.service.CoreUserService;

//@Scope(CoreConstant.SCOPE_PROTOTYPE)
@Component
public class UserValidator implements Validator {

	@Autowired private CoreUserService coreUserService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return CoreUser.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		
//		ValidationUtils.rejectIfEmpty(e, CoreUser.NAME, 	"nameNotEmpty");
//		ValidationUtils.rejectIfEmpty(e, CoreUser.PASSWORD, "passwordNotEmpty");
		
//		CoreUser coreUser = (CoreUser) obj;
//		if (coreUserService.exists(coreUser)) {
//			e.rejectValue(CoreUser.NAME, "nameExists");
//		}
	}

}
