package com.ftl.notebook.core.model.note;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.jackrabbit.value.BinaryValue;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreUser;
import com.ftl.notebook.core.model.embeddable.Rating;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.service.NotebookOakRepoFileSystemImpl;
import com.ftl.oak.model.IRepoNode;
import com.ftl.oak.model.RepoFile;

@Entity
@Table(name= Note.TABLE)
@SequenceGenerator(name = Note.ID_SEQ, sequenceName = Note.SEQ, allocationSize = 1)
@NamedEntityGraphs({
	@NamedEntityGraph(name = Note.ENTITY_GRAPH_WITH_CREATED_BY, attributeNodes = @NamedAttributeNode(Note.CREATED_BY)),
	@NamedEntityGraph(name = Note.ENTITY_GRAPH_WITH_COMMENTS, attributeNodes = @NamedAttributeNode(Note.COMMENTS))
})
public class Note extends CoreModel implements IRepoNode {
	
	public static final String TABLE		= "notebook_note";
	public static final String SEQ 			= "notebook_note_seq";
	
	// Entity Graphs / Fetch Profiles
	public static final String ENTITY_GRAPH_WITH_CREATED_BY = "EG.w.created";
	public static final String ENTITY_GRAPH_WITH_COMMENTS = "EG.w.comments";
	
	// Attributes
	public static final String COURSE 		= "course";
	public static final String RATING		= "rating";
	public static final String LONG_DESC	= "longDesc";
	public static final String COMMENTS		= "comments";
	
	// Column Names
	public static final String COLUMN_LONG_DESC 	= "long_desc";
	
	public Note() {}
	
	@NotNull
	@ManyToOne	
	private Course course;
	
	@NotNull
	@Cascade({CascadeType.ALL})
	@ManyToOne
	private RepoFile file;
	
	@Column(name = COLUMN_LONG_DESC)
	private String longDesc;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = Comment.NOTE)
	private Set<Comment> comments = new HashSet<Comment>();
	
	@Embedded
	private Rating rating = new Rating();
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getLongDesc() {
		return longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public RepoFile getFile() {
		return file;
	}

	public void setFile(RepoFile file) {
		this.file = file;
	}
	
	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	// Utility Functions
	public BinaryValue getData() throws IOException {
		return new BinaryValue(getFile().getMultipartFile().getInputStream());
	}
	
	@Override
	@NotNull
	public CoreUser getCreatedBy() {
		return super.getCreatedBy();
	}

	// IRepoNode implementation
	@Override
	public String getNodeName() {
		return String.valueOf(id);
	}

	@Override
	public Optional<IRepoNode> getParentNode() {
 		return Optional.of(course);
	}
	
	@Override
	public String getNodeFolderName() {
		return NotebookOakRepoFileSystemImpl.FOLDER_NOTES;
	}

}