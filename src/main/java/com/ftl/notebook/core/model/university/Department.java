package com.ftl.notebook.core.model.university;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfile.FetchOverride;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import com.ftl.core.model.CoreModel;
import com.ftl.notebook.core.service.NotebookOakRepoFileSystemImpl;
import com.ftl.oak.model.IRepoNode;

@Entity
@Table(name = Department.TABLE)
@SequenceGenerator(name = Department.ID_SEQ, sequenceName = Department.SEQ, allocationSize = 1)
@FetchProfile(
	name = Department.FETCH_PROFILE_WITH_COURSES,
	fetchOverrides={
		@FetchOverride(
			entity=Department.class,
			association= Department.COURSES,
			mode= FetchMode.JOIN
		)
	}
)
public class Department extends CoreModel implements IRepoNode {
	
	public static final String TABLE		= "notebook_department";
	public static final String SEQ 			= "notebook_department_seq";
	
	// Entity Graphs / Fetch Profiles
	public static final String FETCH_PROFILE_WITH_COURSES = "FP.w.courses";
	
	// Attributes
	public static final String UNIVERSITY 	= "university";
	public static final String COURSES		= "courses";
	public static final String CODE			= "code";
	public static final String WEB_PAGE 	= "webPage";
	
	// Column Names
	public static final String COLUMN_WEB_PAGE 	= "web_page";
	
	public Department() {}
	
	@NotNull
	@ManyToOne
	@NaturalId
	private University university;
	
	@NotEmpty
	@Column
	@NaturalId
	protected String code;
	
	@URL
	@Column(name = COLUMN_WEB_PAGE)
	private String webPage;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = Course.DEPARTMENT)
	private Set<Course> courses = new HashSet<Course>();
	
	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getWebPage() {
		return webPage;
	}
	
	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}
	
	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	// Utility Functions
	// IRepoNode implementation
	@Override
	public String getNodeName() {
		return code;
	}
	
	@Override
	public Optional<IRepoNode> getParentNode() {
		return Optional.of(university);
	}
	
	@Override
	public String getNodeFolderName() {
		return NotebookOakRepoFileSystemImpl.FOLDER_DEPARTMENTS;
	}
	
}
