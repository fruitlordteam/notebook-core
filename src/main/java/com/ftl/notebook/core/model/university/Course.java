package com.ftl.notebook.core.model.university;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfile.FetchOverride;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.NotEmpty;

import com.ftl.core.model.CoreModel;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.service.NotebookOakRepoFileSystemImpl;
import com.ftl.oak.model.IRepoNode;

@Entity
@Table(name = Course.TABLE)
@SequenceGenerator(name = Course.ID_SEQ, sequenceName = Course.SEQ, allocationSize = 1)
@FetchProfile(
	name = Course.FETCH_PROFILE_WITH_NOTES,
	fetchOverrides={
		@FetchOverride(
			entity=Course.class,
			association= Course.NOTES,
			mode= FetchMode.JOIN
		)
	}
)
public class Course extends CoreModel implements IRepoNode {
	
	public static final String TABLE		= "notebook_course";
	public static final String SEQ 			= "notebook_course_seq";
	
	// Entity Graphs / Fetch Profiles
	public static final String FETCH_PROFILE_WITH_NOTES = "FP.w.notes";
	
	// Attributes
	public static final String DEPARTMENT 	= "department";
	public static final String NOTES		= "notes";
	public static final String CODE			= "code";
	
	public Course() {}
	
	@NotNull
	@ManyToOne
	@NaturalId
	private Department department;

	@NotEmpty
	@Column
	@NaturalId
	protected String code;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = Note.COURSE)
	private Set<Note> notes = new HashSet<Note>();
	
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<Note> getNotes() {
		return notes;
	}

	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}

	// Utility Functions
	// IRepoNode implementation
	@Override
	public String getNodeName() {
		return code;
	}

	@Override
	public Optional<IRepoNode> getParentNode() {
 		return Optional.of(department);
	}
	
	@Override
	public String getNodeFolderName() {
		return NotebookOakRepoFileSystemImpl.FOLDER_COURSES;
	}
}
