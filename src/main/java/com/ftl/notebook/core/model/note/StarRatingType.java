package com.ftl.notebook.core.model.note;

import com.ftl.core.utils.CoreConstant;

/**
 * Enumeration for Star Ratings (1-5)
 * Use with star-rating.js/css
 * @author Petros Siatos
 *
 */
public enum StarRatingType {
	
	STARS_0("0"),
	STARS_1("1"), STARS_1_5("1.5"),   
	STARS_2("2"), STARS_2_5("2.5"), 
	STARS_3("3"), STARS_3_5("3.5"),
	STARS_4("4"), STARS_4_5("4.5"),
	STARS_5("5");

	private static final String STARS = "STARS";
	
	/**
	 * Function that initializes star ratings.
	 * in file star-rating.js
	 */
	public static final String JAVASCRIPT_FUNCTION_INITIALIZE = "srInitialize";
	
	private String value;

	private StarRatingType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public float getFloatValue() {
		return Float.valueOf(value);
	}
	
	public static StarRatingType getFloorStarRating(float value) {
		return valueOf(Math.floorDiv(Math.round(value * 10), 5) * 0.5f);
	}
	
	public static StarRatingType valueOf(float value) {
		// Send Natural numbers to valueOf(int)
		// e.g. To avoid a value of 1.0 to return 'STARS_1.0' which will throw an exception. 
		if ((int)value == value) return valueOf((int) value);
		
		String stringValue = String.valueOf(value).replace(CoreConstant.STR_DOT, CoreConstant.STR_UNDERSCORE);
		
		return valueOf(String.join(CoreConstant.STR_UNDERSCORE, STARS, stringValue));
	}
	
	public static StarRatingType valueOf(int value) {
		return valueOf(String.join(CoreConstant.STR_UNDERSCORE, STARS, String.valueOf(value)));
	}

	@Override
	public String toString() {
		return getValue();
	}

}
