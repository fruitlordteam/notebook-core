package com.ftl.notebook.core.model.note;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreUser;
import com.ftl.core.utils.CoreConstant;

/**
 * Note Rating per User
 * 
 * @author Petros Siatos
 *
 */
@Entity
@Table(name= NoteRating.TABLE)
@SequenceGenerator(name = NoteRating.ID_SEQ, sequenceName = NoteRating.SEQ, allocationSize = 1)
public class NoteRating extends CoreModel {

	public static final String TABLE		= "notebook_note_rating";
	public static final String SEQ 			= "notebook_note_rating_seq";
	
	@NotNull
	@ManyToOne	
	public Note note;
	
	@NotNull
	@ManyToOne	
	public CoreUser user;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	public StarRatingType rating;

	public NoteRating() {
		super();
	}
	
	public NoteRating(Note note, CoreUser user, StarRatingType rating) {
		super();
		this.note = note;
		this.user = user;
		this.rating = rating;
	}

	public Note getNote() {
		return note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public CoreUser getUser() {
		return user;
	}

	public void setUser(CoreUser user) {
		this.user = user;
	}

	public StarRatingType getRating() {
		return rating;
	}

	public void setRating(StarRatingType rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return String.join(CoreConstant.STR_SPLITTER_1, note.toString(), user.toString(), rating.toString());
	}

}
