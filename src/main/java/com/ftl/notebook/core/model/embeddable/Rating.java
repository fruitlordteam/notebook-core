package com.ftl.notebook.core.model.embeddable;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import com.ftl.notebook.core.model.note.StarRatingType;

/**
 * Model to keep Rating info (could be used for likes as well.)
 * @author Petros Siatos
 *
 */
@Embeddable
public class Rating {

	// Attributes
	public static final String COLUMN_TOTAL	= "rating_total";
	public static final String COLUMN_SUM	= "rating_sum";
	public static final String RATING_APPROX= "rating.approxRating";
	
	public static final String FORMULA = "(CASE rating_total"
											+ " WHEN 0 THEN 0"
											+ " ELSE rating_sum / rating_total"
										+ " END)";
	
//	public static final DecimalFormat precision = new DecimalFormat("##.00");
	public static final int PRECISION = 100;
	
	@Column(name = COLUMN_TOTAL)
	public float total = 0.0f;
	
	@Column(name = COLUMN_SUM)
	public float sum = 0.0f;
	
	@Transient
	public StarRatingType starRating;
	
	@Formula(FORMULA)
	public float approxRating;

	public float getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public float getSum() {
		return sum;
	}

	public void setSum(float sum) {
		this.sum = sum;
	}

	public StarRatingType getStarRating() {
		return StarRatingType.getFloorStarRating(approxRating);
	}

	public void setStarRating(StarRatingType starRating) {
		this.starRating = starRating;
	}
	
	public float getApproxRating() {
		return Math.round(approxRating * PRECISION) / PRECISION;
	}

	public void setApproxRating(float approxRating) {
		this.approxRating = approxRating;
	}

	//
	// Utility Functions
	//
	public void addStarRating(StarRatingType starRating) {
		total++;
		sum += starRating.getFloatValue();
	}

	@Override
	public String toString() {
		return getStarRating().toString();
	}
	
	
}
