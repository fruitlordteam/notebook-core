package com.ftl.notebook.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ftl.notebook.core.model.note.StarRatingType;

public class Note {

	@Test
	public void mathLearn() {
		
		float x = 2.433232f;
		float y = 2.733232f;
		
		assertEquals(StarRatingType.STARS_2, StarRatingType.getFloorStarRating(x));
		assertEquals(StarRatingType.STARS_2_5, StarRatingType.getFloorStarRating(y));
		assertEquals(StarRatingType.STARS_1, StarRatingType.getFloorStarRating(1.2f));
		assertEquals(StarRatingType.STARS_5, StarRatingType.getFloorStarRating(5f));
	}
	
}