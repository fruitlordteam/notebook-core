package com.ftl.notebook;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.ftl.notebook.config.NotebookTestConfig;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.service.CourseService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = NotebookTestConfig.class)
@Transactional
public class CourseTest {
	
	@Autowired
	private CourseService courseService;
	
	@PersistenceContext
    EntityManager em;
	
//	@Autowired
//    private LocalValidatorFactoryBean validator;
	
//	@Test(expected = ValidationException.class)
	public void validCourse1() {
		Course c = new Course();
		c.setDescription("not");
		courseService.save(c);
		em.flush();
	}
	
}
