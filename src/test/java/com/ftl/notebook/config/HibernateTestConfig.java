package com.ftl.notebook.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ftl.core.utils.CoreConstant;

@Configuration
@EnableTransactionManagement
public class HibernateTestConfig {

	public static final String[] PATH_ENTITIES = new String[] {"com.ftl.core.model", "com.ftl.notebook.core.model", "com.ftl.oak.model"};
	public static final String BEAN_DATASOURCE = "dataSource";
	
	private static final String PROPERTY_DB_URL 	= "db.url";
	private static final String PROPERTY_DB_USER 	= "db.user";
	private static final String PROPERTY_DB_PASS	= "db.pass";
	
	private static final String PROPERTY_DB_SHOW_SQL	= "db.show.sql";
	private static final String PROPERTY_DB_AUTO_DDL	= "db.hibernate.auto";
	
	@Autowired
	Environment env;
	
	@Bean(name = BEAN_DATASOURCE)
	@Profile(CoreConstant.PROFILE_TEST)
	public DataSource dataSource() {

		PGPoolingDataSource ds = new PGPoolingDataSource();
//		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUser(env.getProperty(PROPERTY_DB_USER));
		ds.setPassword(env.getProperty(PROPERTY_DB_PASS));
		ds.setUrl(env.getProperty(PROPERTY_DB_URL));
		return ds;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		// TODO: find a better way to include other project entities
		em.setPackagesToScan(PATH_ENTITIES);
		
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}
	
	private Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", env.getProperty(PROPERTY_DB_AUTO_DDL));
//		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		return properties;
	}
	
}
