package com.ftl.notebook.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ftl.core.config.CoreConfig;

@Configuration
@ComponentScan({NotebookTestConfig.PACKAGE_ROOT})
@EnableJpaRepositories(basePackages = {NotebookTestConfig.PACKAGE__DAO}, repositoryImplementationPostfix = NotebookTestConfig.REPOSITORY_IMPL_POSTFIX)
@Import({ CoreConfig.class, HibernateTestConfig.class })
public class NotebookTestConfig {
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	public static final String PACKAGE_ROOT = "com.ftl.notebook.core";
	public static final String PACKAGE__DAO = "com.ftl.notebook.core.dao";
	public static final String REPOSITORY_IMPL_POSTFIX = "Impl";
}